# Permissions

## Idea

On a classical Unix-System 12 bits are used to control access to file. While modern Linux filesystems offer additional feature for access control (capabilities, ACLs) these traditional access control mechanism is still in use.

This article deals with the question how many of the 2^12=4,096 possible combinations are in use and what we can learn from these.

## Setup

We install an amd64 Debian 12 ("bookworm") system with debian-12.5.0-amd64-netinst.iso - all files in one partition - and select Debian desktop Environment/GNOME, SSH server and standard system utilities.

As the desktop environment starts some daemons that open Unix domain sockets we start our system to multiuser mode:

`systemctl set-default multi-user.target`

## Analysis

We see 163,107 files in total - the number might slightly change throughout this article because the system is living.

As a first analysis has shown that there are more different permissions used on pseudo filesystems we first find out how many files of each possible filetype we have on our system by issuing the command

`find / -mount -printf '%y\n' | sort | uniq -c | sort -nr | cat -n`

as user root.

`find` runs through the whole root partition, `-mount` inhibts it from descending into pseudo filesystems (and `/boot/efi`). For each directory entry found it prints the type of the file. We then `sort` this output because `uniq` only works on adjacent lines. `-c` counts the adjacent lines, we `sort` by this count and prepend a number to each output line.

The result is
```
     1	 133174 f
     2	  18882 l
     3	  11051 d
```

The high number of symbolic links (~11.6%) is a surprise.

We now run a similiar `find` - just instead of the filetype we print out the octal mode, always with four digits:

`find / -mount -printf '%.4m\n' | sort | uniq -c | sort -nr | cat -n`

The result is
```
     1	 126279 0644
     2	  18882 0777
     3	  17693 0755
     4	     90 0700
     5	     58 0600
     6	     25 0640
     7	     18 1777
     8	     14 4755
     9	     12 2755
    10	      8 0744
    11	      3 2775
    12	      3 0775
    13	      2 4754
    14	      2 2750
    15	      2 0711
    16	      2 0710
    17	      2 0664
    18	      2 0555
    19	      2 0444
    20	      2 0440
    21	      1 6755
    22	      1 3775
    23	      1 1770
    24	      1 1730
    25	      1 0770
    26	      1 0660
```

So, 26 out of the possible 4,096 permissions are used. Let us have a further look onto them.

### `0644`

A file with octal mode 0644 ist readable and writable by it's owner and readable by all other users (provided they have access to the directory where the file resides in) - and indeed all entries with mode 0644 are regular files as can be verified by

`find / -mount -perm 644 -printf '%y\n' | sort | uniq -c | sort -nr | cat -n`

This looks reasonable.

### `0777`

All files having mode 0777 are symbolic links as can be verified with a `find` similiar to the last one.

Again, this looks reasonable.

### `0755`

There are two types of directoy entries where an octal mode of 0755 makes sense:
1. For directories this mode is the equivalent to 0644 for regular files - keep in mind that you need to have the execute bit to get access on the inode of an directory entry.
2. Standard executables have this mode.

So, it is no surprise that 10,925 directories and 6,768 files have this mode.

### `0700`

88 of the 90 directory entries are directories themselves - it is reasonable to restrict access to a directory to to the owner:

The 2 files with this mode are
```
/usr/lib/cups/backend/cups-brf
/usr/lib/cups/backend/implicitclass
```

⚠We've no explanation why – on an open source system – a file shouldn't be readable and executable by everyone. 

We are curios for an explantion!

### `0600`

Like directories with 0700 files with 0600 look perfectly ok.

### `1777`

The sticky bit is only meaningful for directories – when it is set only the creator of file in such a directory can delete it.

`/tmp` and `/var/tmp` are commonly known to have this mode, besides these X11 and systemd seem to use it.

Looks fine.

### `4755`

Since the setuid bit has no meaning for directories it is no surprise that we have only files with this mode.

They all belong to root – so they are the potentially dangerous setuid root programs. Thanks to capabilities their numbers could be reduced 🙂

### `2755`

The 10 regular files with this mode are comparable to the section before – but instead of running as owner of the file they are run as group of the file. Still dangerous but not as dangerous as their counterparts. 

We found 2 directories with with mode, `/var/log/journal` and a subdirectory of it. This is no surprise since the setgid in directories includes that all entries created there inherit the property.

The files in the subdirectory have ACLs set.

⚠ This requires further analysis!

### `0744`

There are only regular files having this mode, and they all reside in `/usr/lib/cups/backend` – the same directory where we found 2 `0700` files...

This really looks broken – a user can't execute these files directly but instead could make a copy and make this copy executable.

### `2775`

There are 3 directories with this mode – group members can work in these directories without restrictions and newly created directories entries inherit the group from their parent.

```
/usr/local/share/fonts
/var/local
/var/mail
```

Looks reasonable.

### `0775`

Again, there are 3 directories with this mode. I consider it a little confusing that the first 2 oft them belong to user root and group root.

```
/var/lib/AccountsService
/var/lib/AccountsService/icons
/var/cache/cups/rss
```

Looks reasonable, but it seems open why the first 2 have the group write bit set and why the third is setgid?

### `4754`

These two executables have mode `4754`:

```
/usr/sbin/pppd                              # owner root, group dip
/usr/lib/dbus-1.0/dbus-daemon-launch-helper # owner root, group messagebus
```

OK, obviously members of group dip/messagebus should be able execute these programs as root.

⚠ But why should the others be able to read the files? Is this just so that everyone could analyze the code – or is there something "more"?

### `2750`

There are two directories with this mode, both belonging to group `dip`:

```
/etc/ppp/peers
/etc/chatscripts
```

Only root can create entries in those directories, but they automatically belong to group `dip`. If they file mode is adapted appropriately this might make sense.

### `0711`

There are two directories with this mode:
```
/var/log/gdm3
/var/lib/gdm3
```

The owner can list the files in these directories and create/delete files there but all others only can access files there if they know their names.

⚠ It looks fine but we would really like to know how this is used?

### `0710`

Here we again have 2 directories with this mode:
```
/etc/ssl/private
/var/spool/cups
```

In contrast to `0711` only members of the group owning the directories can access files if they have their names – again we are curios to see how this is used.

### `0664` and `0660`

It was a surprise that only three files have are group writable the all belong to user root and group utmp

```
0664 root utmp /var/log/wtmp
0660 root utmp /var/log/btmp
0664 root utmp /var/log/lastlog
```

So, a normal user can see who logged in successful, but not the login failures.

### `0555`

The 2 "mount points" `/proc` and `/sys` have this mode.

### `0444` and `0440`

The two regular files `/usr/lib/udev/hwdb.bin` and `/etc/machine-id` are only readable for everyone including the owner – since the owner of both files is root, this is only an hint – but at least editors make is more difficult to overwrite the file accidentally. Regarding the reason why these files exists it looks reasonable to give this hint (see `man machine-id` and `man hwdb`).

The two regular files `/etc/sudoers` and `/etc/sudoers.d/README` – while the "editor argument" in last paragraph holds true for the first file, the read protection for the latter looks somewhat strange.

### `6755`

`/usr/lib/xorg/Xorg.wrap` is the file with the highest octal mode - congratulations 🙂.

Curios, if the setgid is really needed...

### `3775`

The directory `/usr/share/ppd/custom` owned by user root and group lpadmin – so members of lpadmin can create files there (because of setgid) but only those users created files can remove them (because of sTicky).

Interesting, but not unreasonable...

### `1770`

`/var/spool/cups/tmp` is owned by root/lp - so only members of lp (and, of coures, user root) can create files there but only the creator can delete a file.

### `1730`

`/var/spool/cron/crontabs` is owned by root/crontab - so again only members of crontab (and, of coures, user root) can create files there but only the creator can delete a file.

In contrast to files in the directory in the last paragraph members of the group event can't see which files are in the directory.

The difference is interesting...

### `0770`

`/var/cache/cups` is owned by root/lp – the members of group lp (in addition to user root) can create and remove files.